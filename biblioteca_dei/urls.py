from django.conf.urls import patterns, include, url
from django.views.generic.simple import direct_to_template , redirect_to
from biblioteca_dei import settings
from tastypie.api import Api
from reservation.api import UserResource, AuthorResource, ProductResource, ReservationResource, BookResource , VideoResource,Reservation_getUserResource

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

reservation_api = Api(api_name='reservation')
reservation_api.register(UserResource())
reservation_api.register(AuthorResource())
reservation_api.register(ProductResource())
reservation_api.register(VideoResource())
reservation_api.register(BookResource())
reservation_api.register(Reservation_getUserResource())
reservation_api.register(ReservationResource())


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'biblioteca_dei.views.home', name='home'),
    # url(r'^biblioteca_dei/', include('biblioteca_dei.foo.urls')),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
	url(r'^admin/', include(admin.site.urls)), 

    # Uncomment the next line to enable the admin:
    (r'^api/', include(reservation_api.urls)),
    (r'^site_media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT}),
    # Examples:,
)
