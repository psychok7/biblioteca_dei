from reservation.models import Author,Reservation , Video, Book
from django.contrib import admin

admin.site.register(Author)
admin.site.register(Video)
admin.site.register(Book)
admin.site.register(Reservation)
