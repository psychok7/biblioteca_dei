from django.contrib.auth.models import User
from tastypie import fields
from tastypie.authentication import BasicAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie.resources import ModelResource
from tastypie.cache import SimpleCache
from reservation.models import Author, Product, Reservation, Video, Book

class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'auth/user'
        fields = ['id','username']
        authentication = BasicAuthentication()
        authorization = DjangoAuthorization()
        #cache = SimpleCache(timeout=10)

class AuthorResource(ModelResource):
    #user = fields.ForeignKey(UserResource, 'user')
    class Meta:
        queryset = Author.objects.all()
        resource_name = 'author'
        authentication = BasicAuthentication()
        authorization = DjangoAuthorization()
        
class ProductResource(ModelResource):
    author = fields.ForeignKey(AuthorResource, 'author')
    class Meta:
        queryset = Product.objects.all()
        resource_name = 'product'
        authentication = BasicAuthentication()
        authorization = DjangoAuthorization()

class VideoResource(ModelResource):
    author = fields.ForeignKey(AuthorResource, 'author')
    class Meta:
        queryset = Video.objects.all()
        resource_name = 'video'
        authentication = BasicAuthentication()
        authorization = DjangoAuthorization()

class BookResource(ModelResource):
    author = fields.ForeignKey(AuthorResource, 'author')
    class Meta:
        queryset = Book.objects.all()
        resource_name = 'book'
        authentication = BasicAuthentication()
        authorization = DjangoAuthorization()

class ReservationResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')
    product = fields.ForeignKey(ProductResource, 'product')
    class Meta:
        queryset = Reservation.objects.all()
        resource_name = 'reservation'
        authentication = BasicAuthentication()
        authorization = DjangoAuthorization()

class Reservation_getUserResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')
    product = fields.ForeignKey(ProductResource, 'product')
    class Meta:
        queryset = Reservation.objects.all()
        resource_name = 'reservation_getUser'
        fields = ['user','product','id']
        authentication = BasicAuthentication()
        authorization = DjangoAuthorization()