from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Author(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()

    def __unicode__(self):
        return self.name

class Product(models.Model):
    author = models.ForeignKey(Author)

    name = models.CharField(max_length=100)
    genre = models.CharField(max_length=100)
    pub_date = models.DateTimeField()
    count = models.IntegerField()


    def __unicode__(self):
        return self.name

class Book(Product):
    pass

class Video(Product):
    pass

class Reservation(models.Model):
    user = models.ForeignKey(User)
    product = models.ForeignKey(Product)

    reserv_date_start = models.DateTimeField()
    reserv_finish = models.DateTimeField()
    penalty = models.BooleanField()

    def __unicode__(self):
        return self.product.name

